'use strict';

var aws   = require('aws-sdk');

var DOC       = require('dynamodb-doc');
var docClient = new DOC.DynamoDB();


// Your first function handler
module.exports.hello1 = (event, context, cb) => cb(null,
  { message: 'Go Serverless v1.0! Your function executed successfully!', event }
);


module.exports.hello = function(event, context, cb) {
	console.log(JSON.stringify(event))
	//var body = event.body

console.log("---")
console.log(event.profile)
console.log(event.name)

console.log("-------context.user----------")
console.log(context.user)
console.log("-----------------")

    docClient.putItem({
        TableName : "stacksavings-facebook-connections",
        Item : {
            profile: event.profile,
            name : event.name,
            gender : event.gender,
            email : event.email,
            authtoken : event.authtoken,
            time : getDateTime(),
            pageurl : event.pageurl,
            ip : event.useripaddress

        }
    }, function(err,data){
    	context.fail(err)
    });

};

function getDateTime() {

    var date = new Date();

    var hour = date.getHours();
    hour = (hour < 10 ? "0" : "") + hour;

    var min  = date.getMinutes();
    min = (min < 10 ? "0" : "") + min;

    var sec  = date.getSeconds();
    sec = (sec < 10 ? "0" : "") + sec;

    var year = date.getFullYear();

    var month = date.getMonth() + 1;
    month = (month < 10 ? "0" : "") + month;

    var day  = date.getDate();
    day = (day < 10 ? "0" : "") + day;

    return year + ":" + month + ":" + day + ":" + hour + ":" + min + ":" + sec;

}


module.exports.read = function(event, context, cb) {

	var docClientAws = new aws.DynamoDB.DocumentClient();

	//var body = event.body
	var body = event


/*	var params = {
	    TableName : "stacksavings-facebook-connections",
	    ProjectionExpression:"#at, email",
	    KeyConditionExpression: "#at = :key",
	    ExpressionAttributeNames:{
	        "#at": "authtoken"
	    },
	    ExpressionAttributeValues: {
	        ":key":body.authtoken
	    } 
	};*/

//	  "AttributesToGet": ['profile','email'],

	var params = {
	  "TableName":"stacksavings-facebook-connections",
	  "FilterExpression": '#key in (:val1)',
	  "ExpressionAttributeNames": {
	    '#key': 'authtoken'
	  },
	  "ExpressionAttributeValues": {
	    ':val1': body.authtoken
	  }
	}


	docClientAws.scan(params, function(err, data) {
	    if (err) {
		context.fail(err)
	    } else {

		context.succeed(data.Items);

	    }
	});

};
